package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"regexp"

	"github.com/xanzy/go-gitlab"
	"gopkg.in/yaml.v2"
)

var (
	debugMode = false
)

// Config configuration
type Config struct {
	Projects map[string]IntegrationConfig `yaml:"projects"`
}

// IntegrationConfig represents the configutaion of a single integration
type IntegrationConfig struct {
	WebhookVar                string `yaml:"webhook"`
	Channel                   string `yaml:"channel"`
	BranchesToBeNotified      string `yaml:"branches_to_be_notified"`
	NotifyOnlyBrokenPipelines bool   `yaml:"notify_only_broken_pipelines"`
}

func main() {
	log.SetFlags(0)
	dryRun := false

	force := flag.Bool("force", false, "force applying the configuration even if it's already there")
	configFile := flag.String("config", "config.yml", "set the configuration file")

	flag.BoolVar(&debugMode, "debug", false, "enable debug mode")
	flag.BoolVar(&dryRun, "dry-run", false, "enable dry-run mode")

	flag.Parse()

	token := MustGetEnv("GITLAB_TOKEN")
	gitlabBaseURL := os.Getenv("GITLAB_BASEURL")

	b, err := ioutil.ReadFile(*configFile)
	if err != nil {
		log.Fatalf("failed to read config file %s: %s", *configFile, err)
	}

	conf := Config{}
	err = yaml.UnmarshalStrict(b, &conf)
	if err != nil {
		log.Fatalf("failed to parse config file: %s", err)
	}

	cli := gitlab.NewClient(nil, token)
	if gitlabBaseURL != "" {
		cli.SetBaseURL(gitlabBaseURL)
	}

	glSvc := NewGitlabService(cli, dryRun)

	c := Configurator{
		Config:        conf,
		GitlabService: glSvc,
		Force:         *force,
	}
	if err := c.Configure(); err != nil {
		log.Fatalf("failed to configure stuff: %s", err)
	}
}

// Configurator configures stuff
type Configurator struct {
	Config        Config
	GitlabService GitlabService
	Force         bool
}

// Configure configures stuff
func (c Configurator) Configure() error {
	projects, err := c.GitlabService.ListProjects()
	if err != nil {
		return fmt.Errorf("could not list Gitlab projects: %s", err)
	}

	for _, p := range projects {
		if err := c.Apply(p); err != nil {
			log.Printf("failed to apply project %s: %s", p.PathWithNamespace, err)
		}
	}

	return nil
}

// Apply applies stuff
func (c Configurator) Apply(p *gitlab.Project) error {
	for pattern, integration := range c.Config.Projects {
		m, err := regexp.MatchString(pattern, p.PathWithNamespace)
		if err != nil {
			return err
		}
		if !m {
			continue
		}

		debug("project %s matched", p.PathWithNamespace)

		slackHook := MustGetEnv(integration.WebhookVar)

		var channel *string
		if integration.Channel != "" {
			channel = &integration.Channel
		}

		var branches *string
		if integration.BranchesToBeNotified != "" {
			branches = &integration.BranchesToBeNotified
		}

		opt := gitlab.SetSlackServiceOptions{
			WebHook:                   &slackHook,
			NotifyOnlyBrokenPipelines: &integration.NotifyOnlyBrokenPipelines,
			DeploymentChannel:         channel,
			IssueChannel:              channel,
			MergeRequestChannel:       channel,
			ConfidentialIssueChannel:  channel,
			TagPushChannel:            channel,
			NoteChannel:               channel,
			PipelineChannel:           channel,
			PushChannel:               channel,
			WikiPageChannel:           channel,
			BranchesToBeNotified:      branches,
		}

		serv, err := c.GitlabService.GetSlackService(p.ID)
		if err != nil {
			return fmt.Errorf("failed to get slack configuration for project %s: %s", p.PathWithNamespace, err)
		}

		if serv.Active {
			if !c.Force && !configChanged(opt, serv.Properties) {
				log.Println(p.PathWithNamespace, "slack integration is correctly configured already")
				return nil
			}
			debug("  service is active, but configuration seems to have changed")
		}

		err = c.GitlabService.SetSlackService(p.ID, &opt)
		if err != nil {
			return fmt.Errorf("failed to set slack configuration for project %s: %s", p.PathWithNamespace, err)
		}

		log.Println(p.PathWithNamespace, "slack integration is now configured")
	}
	log.Println(p.PathWithNamespace, "does not match any configuration")

	return nil
}

// MustGetEnv either gets the environment variable, or crashes the app
func MustGetEnv(key string) string {
	value := os.Getenv(key)
	if value == "" {
		log.Fatalf("Environment variable %s is mandatory", key)
	}
	return value
}

func configChanged(a gitlab.SetSlackServiceOptions, b *gitlab.SlackServiceProperties) bool {
	if b == nil {
		debug("  project has no service properties")
		return true
	}

	if a.WebHook != nil && *a.WebHook != b.WebHook {
		debug("  project has different webhook")
		return true
	}
	// if a.DeploymentChannel != nil && *a.DeploymentChannel != b.DeploymentChannel {
	// 	debug("  project has different deployment channel %s vs %s", *a.DeploymentChannel, b.DeploymentChannel)
	// 	return true
	// }
	// if a.IssueChannel != nil && *a.IssueChannel != b.IssueChannel {
	// 	debug("  project has different issue channel %s vs %s", *a.IssueChannel, b.IssueChannel)
	// 	return true
	// }
	// if a.MergeRequestChannel != nil && *a.MergeRequestChannel != b.MergeRequestChannel {
	// 	debug("  project has different merge request channel %s vs %s", *a.MergeRequestChannel, b.MergeRequestChannel)
	// 	return true
	// }
	// if a.ConfidentialIssueChannel != nil && *a.ConfidentialIssueChannel != b.ConfidentialIssueChannel {
	// 	debug("  project has different confidential issue channel %s vs %s", *a.ConfidentialIssueChannel, b.ConfidentialIssueChannel)
	// 	return true
	// }
	// if a.TagPushChannel != nil && *a.TagPushChannel != b.TagPushChannel {
	// 	debug("  project has different tag push channel %s vs %s", *a.TagPushChannel, b.TagPushChannel)
	// 	return true
	// }
	// if a.NoteChannel != nil && *a.NoteChannel != b.NoteChannel {
	// 	debug("  project has different channel %s vs %s", *a.Channel, b.Channel)
	// 	return true
	// }
	// if a.PipelineChannel != nil && *a.PipelineChannel != b.PipelineChannel {
	// 	debug("  project has different pipeline channel %s vs %s", *a.PipelineChannel, b.PipelineChannel)
	// 	return true
	// }
	// if a.PushChannel != nil && *a.PushChannel != b.PushChannel {
	// 	debug("  project has different push channel %s vs %s", *a.PushChannel, b.PushChannel)
	// 	return true
	// }
	// if a.WikiPageChannel != nil && *a.WikiPageChannel != b.WikiPageChannel {
	// 	debug("  project has different wiki page channel %s vs %s", *a.WikiPageChannel, b.WikiPageChannel)
	// 	return true
	// }

	// if a.BranchesToBeNotified != nil && *a.BranchesToBeNotified != b.BranchesToBeNotified {
	// 	debug("  project has different branches to be notified %s vs %s", *a.BranchesToBeNotified, b.BranchesToBeNotified)
	// 	return true
	// }

	// if a.NotifyOnlyBrokenPipelines != nil && *a.NotifyOnlyBrokenPipelines != bool(b.NotifyOnlyBrokenPipelines) {
	// 	debug("  project has different broken pipelines %t vs %t", *a.NotifyOnlyBrokenPipelines, bool(b.NotifyOnlyBrokenPipelines))
	// 	return true
	// }

	return false
}

func debug(format string, v ...interface{}) {
	if debugMode {
		log.Printf(format, v...)
	}
}
