# Back to slack

Configures the slack notification integration for all the projects that match.

## Usage

`./back-to-slack [-force]`

## Environment variables

### GITLAB_TOKEN

The token used to talk to gitlab API, required

### GITLAB_BASEURL

The url in which gitlab API is, by default gitlab.com

## How to configure

```yml
---
projects:
  group/repo.*:
    webhook: ENV_VAR_FOR_SLACK_URL
    channel: "#the_channel" # optional
    branches_to_be_notified: default # by default, valid values are “all”,
                             # “default”, “protected”, and “default_and_protected”
    notify_only_broken_pipelines: false
```
